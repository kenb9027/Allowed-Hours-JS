var weekDate = [
    monday = {
        id: 1,
        name: "Lundi",
        isOpen: true,
        openhours: [
            ['11: 30', '13: 30'],
            ['18: 30', '21: 00']]
    },
    tuesday = {
        id: 2,
        name: "Mardi",
        isOpen: true,
        openhours: [
            ['11: 30', '13: 30'],
            ['18: 30', '21: 00']]
    },
    wednesday = {
        id: 3,
        name: "Mercredi",
        isOpen: false,
        openhours: []
    },
    thursday = {
        id: 4,
        name: "Jeudi",
        isOpen: true,
        openhours: [
            ['11: 30', '13: 30'],
            ['18: 30', '21: 00']]
    },
    friday = {
        id: 5,
        name: "Vendredi",
        isOpen: true,
        openhours: [
            ['11: 30', '13: 30'],
            ['18: 30', '21: 30']]
    },
    saturday = {
        id: 6,
        name: "Samedi",
        isOpen: true,
        openhours: [
            ['18: 30', '21: 30']]
    },
    sunday = {
        id: 0,
        name: "Dimanche",
        isOpen: false,
        openhours: [
            ['18: 30', '21: 30']]
    }
];
var closedDaysList = [
    ['2020-09-30'], ['2020-09-31'], ['2020-10-2'], ['2020-10-12']
];
var closedPeriodsList = [
    ['2020-09-02/2020-09-29'], ['2020-10-3/2020-10-10']
]
var msInOneDay = 86400000;// milliseconds en 1 journée
function nextDayOpened(weekDate, day) {
    var nextDayId = day.id + 1;
    var count = 1;
    for (let d = 0; d < 6; d++) {
        nextDayId === 7
            ? nextDayId = 0
            : nextDayId
        let nextDay = weekDate.find(element => element.id === nextDayId)
        if (nextDay.isOpen === true) {
            return [nextDay, count];
        }
        else {
            nextDayId += 1;
            count += 1
        }
    }
}
/**
 * Find if this date is in the closedDays list
 * @param {Array} closedDaysList 
 * @param {Date} date 
 * @returns {Boolean}
 */
function isClosedDay(closedDaysList, date, msInOneDay) {
    var nbDate = date.getTime();
    var isIt = false;
    for (closedDay of closedDaysList) {
        var closed = closedDay[0].split('-');
        var nbClosedDay = new Date(parseInt(closed[0]), parseInt(closed[1]), parseInt(closed[2])).getTime();
        if (nbDate >= nbClosedDay && nbDate < nbClosedDay + msInOneDay) {
            isIt = true;
            return isIt;
        }
    }
    return isIt;
}
/**
 * Find if this date is in the closedPeriods list
 * @param {Array} closedPeriodsList 
 * @param {Date} date 
 * @returns {Date} the date of restart if it is || @returns {false} if it's not in closedPeriods ;  
 */
function isClosedPeriod(closedPeriodsList, date, msInOneDay) {

    var nbDate = date.getTime();
    var isIt = false;

    for (closedPeriod of closedPeriodsList) {
        var period = closedPeriod[0];

        var startPeriod = period.split('/')[0];
        var startPeriodMonth = startPeriod.split('-')[1];
        var startPeriodYear = startPeriod.split('-')[0];
        var startPeriodDay = startPeriod.split('-')[2];

        var endPeriod = period.split('/')[1];
        var endPeriodMonth = endPeriod.split('-')[1];
        var endPeriodYear = endPeriod.split('-')[0];
        var endPeriodDay = endPeriod.split('-')[2];

        var nbStartPeriod = new Date(startPeriodYear, startPeriodMonth, startPeriodDay).getTime();
        var nbEndPeriod = new Date(endPeriodYear, endPeriodMonth, endPeriodDay).getTime();

        if (nbDate >= nbStartPeriod && nbDate < nbEndPeriod + msInOneDay) {
            isIt = true;
            var restartPeriodDay = parseInt(endPeriodDay) + 1
            var restartPeriodDate = new Date(endPeriodYear, endPeriodMonth, restartPeriodDay)
            return restartPeriodDate;
        }
    }
    return isIt;
}
/**
 * Find the correspondant day in the weekDate
 * @param {Array} weekDate 
 * @param {Date} date 
 * @returns {Array} opening times for this day of the week
 */
function findOpeningTimes(weekDate, date) {
    // 40 : horaire max de commande pour chaque heure, car on commande de 5min en 5min, 
    // derniere commande à xx:40 pour xx:55, et à xx:41 on commande pour la prochaine heure xx+1:00
    const maxMinOrder = 40;
    const timeBeforeClosing = 15; // no order 15min before the end of the service
    const timeToWait = 15; //minimum time to wait a command
    let ddd = new Date(date).getDay();

    let day = weekDate.find(day => day.id === ddd);
    var allowedDayHours = [];
    if (day.isOpen) {

        let services = day.openhours
        let theDate = new Date(date)
        let hours = theDate.getHours();
        let minutes = theDate.getMinutes();

        for (service of services) {
            let serviceOpenHours = [];
            let serviceStartHour = service[0].split(":")[0];
            let serviceEndHour = service[1].split(":")[0];
            let serviceEndMinute = service[1].split(":")[1].trim();
            let timeRange = serviceEndHour - serviceStartHour
            for (i = 0; i <= timeRange; i++) {
                let openHour = parseInt(serviceStartHour) + i;
                serviceOpenHours.push(openHour);
            }
            if (parseInt(serviceEndMinute) === 0) {
                serviceOpenHours.pop()
            }
            serviceOpenHours.forEach(function (serviceOpenHour) {
                if (serviceOpenHour > hours) {
                    allowedDayHours.push(serviceOpenHour);
                }
                else if (serviceOpenHour === hours) {
                    //dernière commande 15min avant la fermeture 
                    if (parseInt(serviceEndHour) === hours &&
                        parseInt(serviceEndMinute) - timeBeforeClosing - timeToWait >= minutes) {
                        allowedDayHours.push(serviceOpenHour);
                    }
                    else if (parseInt(serviceEndHour) !== hours &&
                        minutes <= maxMinOrder) {
                        allowedDayHours.push(serviceOpenHour);
                    }
                }
            })
        }
    }
    return allowedDayHours;
}

function allowedHours(date, weekDate, closedDaysList, closedPeriodsList, msInOneDay) {
    var orderDate = new Date(date);
    console.log("Date demandée : ");
    console.log(orderDate)

    var day = orderDate.getDay();
    var today = weekDate.find(date => date.id === day);

    while (
        isClosedDay(closedDaysList, orderDate, msInOneDay) ||
        isClosedPeriod(closedPeriodsList, orderDate, msInOneDay) !== false) {

        if (isClosedDay(closedDaysList, orderDate, msInOneDay)) {
            let nd = orderDate.getDate() + 1;
            orderDate.setDate(nd);
        }
        else if (isClosedPeriod(closedPeriodsList, orderDate, msInOneDay) !== false) {
            let newOrderDate = isClosedPeriod(closedPeriodsList, orderDate, msInOneDay);
            orderDate = newOrderDate;
        }
    }
    console.log(orderDate);

    if (today.isOpen && findOpeningTimes(weekDate, orderDate).length !== 0) {

        console.log("Commande possible aujours'hui : ")
        console.log(orderDate)
        console.log(findOpeningTimes(weekDate, orderDate))
    }
    else if (!today.isOpen ||
        (today.isOpen && findOpeningTimes(weekDate, orderDate).length === 0)) {

        var nextOpenDay = nextDayOpened(weekDate, today)[0];
        var dCount = nextDayOpened(weekDate, today)[1];
        var q = orderDate.getDate();
        var s = q + dCount
        var newOd = new Date(orderDate)
        newOd.setDate(s);
        newOd.setHours(0);
        newOd.setMinutes(0);
        newOd.setSeconds(0);
        newOd.setMilliseconds(0);
        orderDate = newOd;

        console.log('Prochaine commande : ')
        console.log(orderDate);
        console.log(findOpeningTimes(weekDate, orderDate))
        // ne prends pas en compte les closedDay et closedPeriod 
    }


}
testAdate = new Date(2020, 9, 19, 21, 30)
//testAdate = new Date(2020, 4, 9, 22, 30)

allowedHours(testAdate, weekDate, closedDaysList, closedPeriodsList, msInOneDay)